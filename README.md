
<img src="https://img.shields.io/packagist/v/agrodata/validator.svg" />
<img src="https://img.shields.io/packagist/dt/agrodata/validator.svg" />

# Agrodata Validator

Este pacote foi criado para agrupar diversas validações utilizadas nos projeto da empresa Agrodata, pois, ao utilizar diversas libs de Validação no laravel/lumen, sempre o último validador instalado subscreve os demais, necessitando de muitas configurações para tornar operável. Desta forma, ficou mais viável e simples criamos nosso próprio pacote com as validações utilizas em nossos projetos.

Basedo no pacote: https://github.com/LaravelLegends/pt-br-validator

|           Regras         | Descrição                                                                    |
|:-------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------:|
| dms                      | Valida se a coordenada é valida e está no seguinte formato:**`N 39º 36' 27,18" / W 009º 04' 21,12"`**                                                               
| latitude                 | Valida se um valor de latitude é valido
| longitude                | Valida se um valor de longitude é valido
| celular                  | Valida se o campo está no formato (**`99999-9999`** ou **`9999-9999`**)                                                                               |
| celular_com_ddd          | Valida se o campo está no formato (**`(99)99999-9999`** ou **`(99)9999-9999`** ou **`(99) 99999-9999`** ou **`(99) 9999-9999`**)                      |
| celular_com_codigo       | Valida se o campo está no formato `+99(99)99999-9999` ou +99(99)9999-9999.                                                                            |
| cnpj                     | Valida se o campo é um CNPJ válido. É possível gerar um CNPJ válido para seus testes utilizando o site [geradorcnpj.com](http://www.geradorcnpj.com/) |
| cpf                      | Valida se o campo é um CPF válido. É possível gerar um CPF válido para seus testes utilizando o site [geradordecpf.org](http://geradordecpf.org)      |
| formato_cnpj             | Valida se o campo tem uma máscara de CNPJ correta (**`99.999.999/9999-99`**).                                                                         |
| formato_cpf              | Valida se o campo tem uma máscara de CPF correta (**`999.999.999-99`**).                                                                              |
| formato_cep              | Valida se o campo tem uma máscara de correta (**`99999-999`** ou **`99.999-999`**).                                                                   |
| telefone                 | Valida se o campo tem umas máscara de telefone (**`9999-9999`**).                                                                                     |
| telefone_com_ddd         | Valida se o campo tem umas máscara de telefone com DDD (**`(99)9999-9999`**).                                                                         |
| telefone_com_codigo      | Valida se o campo tem umas máscara de telefone com DDD (**`+55(99)9999-9999`**).                                                                      |
| formato_placa_de_veiculo | Valida se o campo tem o formato válido de uma placa de veículo (incluindo o padrão MERCOSUL).                                                         |
| formato_pis              | Valida se o campo tem o formato de PIS.                                                                                                               |
| pis                      | Valida se o PIS é válido.                                                                                                                             |
| cpf_ou_cnpj              | Valida se o campo é um CPF ou CNPJ                                                                                                                    |
| formato_cpf_ou_cnpj      | Valida se o campo contém um formato de CPF ou CNPJ                                                                                                    |
| uf                       | Valida se o campo contém uma sigla de Estado válido (UF)                                                                                              |                                                                                             |

## 1. Instalação
```
composer require agrodata/validator
```

## 2. Configuração
#### Lumen Framework

Registre o "Provider" no arquivo ```bootstrap/app.php```

Adicione a seguinte lina na seção "Register Service Providers" no fim do arquivo.

```php
$app->register(\Agrodata\Validator\ValidatorProvider::class);
```
#### Laravel

Adicione a seguinte linha em `config\app.php` na seção "providers".

```
 'providers' => [
    \Agrodata\Validator\ValidatorProvider::class
  ]
```

#### *Extra
*Opcionalmente você pode publicar o arquivo de configuração. Dentro desse arquivo estão todas as expressões regulares (regex) utilizados para as validações feitos no pacote. Deve ser utilizada quando há necessidade de alterar alguma validação.

```
php artisan vendor:publish  --provider="Agrodata\Validator\ValidatorProvider" --tag="config"
```

## 3. Utilização

### 3.1 Requests
Poderá ser utilizado nos arquivos que extendem a classe **`\Illuminate\Foundation\Http\FormRequest`** normalmente os arquivos do diretório **`\App\Http\Requests`** . exemplo:
```php
...
public function rules()
{
    return [
         'person_doc' => ['cnpj']
    ]
}
...
```

### 3.2 Validators
outra forma de utilização é com a classe **`Illuminate\Support\Facades\Validator`**
```php
$isValidCpf = !Validator::make(request()->all(), [$attribute => 'cpf'])->fails();
```

