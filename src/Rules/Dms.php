<?php

namespace Agrodata\Validator\Rules;

use Illuminate\Contracts\Validation\Rule;

class Dms implements Rule
{
    public function passes($attribute, $value)
    {
        return preg_match(config('validator-regex.dms'), $value) > 0;
    }

    public function message()
    {
        return [
            'pt-br' => 'O campo :attribute não é um dms válido.',
            'en'    => 'The field :attribute is not a valid DMS'
        ];
    }
}
