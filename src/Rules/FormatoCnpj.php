<?php

namespace Agrodata\Validator\Rules;

use Illuminate\Contracts\Validation\Rule;

/**
* @author Wallace Maxters <wallacemaxters@gmail.com>
*/
class FormatoCnpj implements Rule
{

    
    /**
     * 
     * Valida o formato do cnpj
     * 
     * @param string $attribute
     * @param string $value
     * @return boolean
    */
    public function passes($attribute, $value)
    {
        return preg_match('/^\d{2}\.\d{3}\.\d{3}\/\d{4}-\d{2}$/', $value) > 0;
    }

    public function message()
    {
        return [
            'pt-br' => 'O campo :attribute não possui o formato válido de CNPJ.',
            'en'    => 'The field :attribute don\'t have a valid CNPJ format'
        ];
    }
}