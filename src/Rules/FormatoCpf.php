<?php

namespace Agrodata\Validator\Rules;

use Illuminate\Contracts\Validation\Rule;

/**
 * @author Wallace Maxters <wallacemaxters@gmail.com>
 */
class FormatoCpf implements Rule
{

    /**
     * Valida o formato do cpf
     * 
     * @param string $attribute
     * @param string $value
     * @return boolean
    */
    public function passes($attribute, $value)
    {
        return preg_match('/^\d{3}\.\d{3}\.\d{3}-\d{2}$/', $value) > 0;
    }

    public function message()
    {
        return [
            'pt-br' => 'O campo :attribute não possui o formato válido de CPF.',
            'en'    => 'The field :attribute don\'t have a valid CPF format'
        ];
    }
}