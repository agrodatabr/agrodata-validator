<?php

namespace Agrodata\Validator\Rules;

use Illuminate\Contracts\Validation\Rule;

class Latitude implements Rule
{
    public function passes($attribute, $value)
    {
        return preg_match(config('validator-regex.latitude'), $value) > 0 && $value >= -90 && $value <= 90;
    }

    public function message()
    {
        return [
            'pt-br' => 'O campo :attribute não é um CPF ou CNPJ válido.',
            'en'    => 'The field :attribute is not a valid Latitude'
        ];
    }
}
