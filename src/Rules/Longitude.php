<?php

namespace Agrodata\Validator\Rules;

use Illuminate\Contracts\Validation\Rule;

class Longitude implements Rule
{
    public function passes($attribute, $value)
    {
        return preg_match(config('validator-regex.longitude'), $value) > 0 && $value >= -180 && $value <= 180;
    }

    public function message()
    {
        return [
            'pt-br' => 'O campo :attribute não é um CPF ou CNPJ válido.',
            'en'    => 'The field :attribute is not a valid Longitude'
        ];
    }
}
