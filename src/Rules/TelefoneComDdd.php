<?php

namespace Agrodata\Validator\Rules;

use Illuminate\Contracts\Validation\Rule;

/**
 * @author Wallace Maxters <wallacemaxters@gmail.com>
 */
class TelefoneComDdd implements Rule
{

    /**
     * Valida o formato do telefone junto com o ddd
     * 
     * @param string $attribute
     * @param string $value
     * @return boolean
    */

    public function passes($attribute, $value)
    {
        return preg_match('/^\(\d{2}\)\s?\d{4}-\d{4}$/', $value) > 0;
    }


    public function message()
    {
        return [
            'pt-br' => 'O campo :attribute não é um telefone com DDD válido.',
            'en'    => 'The field :attribute is not a valid phone number with DDD number'
        ];
    }
}