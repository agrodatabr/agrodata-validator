<?php

namespace Agrodata\Validator;

use Illuminate\Support\ServiceProvider;

class ValidatorProvider extends ServiceProvider
{

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;


    /**
     * Bootstrap the application events.
     *
     * @return void
     */

    public function boot()
    {
        $rules = [
            'latitude'  => \Agrodata\Validator\Rules\Latitude::class,
            'longitude' => \Agrodata\Validator\Rules\Longitude::class,
            'dms'       => \Agrodata\Validator\Rules\Dms::class,

            'celular'                        => \Agrodata\Validator\Rules\Celular::class,
            'celular_com_ddd'                => \Agrodata\Validator\Rules\CelularComDdd::class,
            'celular_com_codigo'             => \Agrodata\Validator\Rules\CelularComCodigo::class,
            'celular_com_codigo_sem_mascara' => \Agrodata\Validator\Rules\CelularComCodigoSemMascara::class,
            'cnh'                            => \Agrodata\Validator\Rules\Cnh::class,
            'cnpj'                           => \Agrodata\Validator\Rules\Cnpj::class,
            'cns'                            => \Agrodata\Validator\Rules\Cns::class,
            'cpf'                            => \Agrodata\Validator\Rules\Cpf::class,
            'formato_cnpj'                   => \Agrodata\Validator\Rules\FormatoCnpj::class,
            'formato_cpf'                    => \Agrodata\Validator\Rules\FormatoCpf::class,
            'telefone'                       => \Agrodata\Validator\Rules\Telefone::class,
            'telefone_com_ddd'               => \Agrodata\Validator\Rules\TelefoneComDdd::class,
            'telefone_com_codigo'            => \Agrodata\Validator\Rules\TelefoneComCodigo::class,
            'formato_cep'                    => \Agrodata\Validator\Rules\FormatoCep::class,
            'formato_placa_de_veiculo'       => \Agrodata\Validator\Rules\FormatoPlacaDeVeiculo::class,
            'formato_pis'                    => \Agrodata\Validator\Rules\FormatoPis::class,
            'pis'                            => \Agrodata\Validator\Rules\Pis::class,
            'cpf_ou_cnpj'                    => \Agrodata\Validator\Rules\CpfOuCnpj::class,
            'formato_cpf_ou_cnpj'            => \Agrodata\Validator\Rules\FormatoCpfOuCnpj::class,
            'uf'                             => \Agrodata\Validator\Rules\Uf::class,
        ];

        foreach ($rules as $name => $class) {
            $rule = new $class;
            $lang = request()->get('language') ?? app()->getLocale() ?? 'en';
            $message = $rule->message()[$lang];

            $extension = static function ($attribute, $value) use ($rule) {
                return $rule->passes($attribute, $value);
            };

            $this->app['validator']->extend($name, $extension, $message);
        }
    }


    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [];
    }
}
